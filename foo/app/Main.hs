module Main where

import Lib
import RIO

main :: IO ()
main = runSimpleApp $ do
  logInfo "Hello World"
  logError "GoodBye"
